# n = print
# n(546546)
# print(56545)
#
# m = lambda n: n * 2
# print(m(5))

import datetime

# def add_log(func, value):
#     print(func(value), ' >>>>> ', datetime.date.today())

# add_log.city = 'Odesa'
# print(add_log.city)
# print(type(add_log))


# def work_if_is_adult(func, arg):
#     age = input('Enter your age')
#     if int(age) > 17:
#         print('I am inside')
#         return func(arg)
#
#     print("You are a child")
#     return False
#
#
# def my_func_myl_2(val):
#     return val * 2
#
#
# add_log(my_func_myl_2, 6)
# add_log(my_func_myl_2, 'jdg')
# add_log(my_func_myl_2, [5, 6])

# work_if_is_adult(my_func_myl_2, 555)



#################################################################################
#  func -> func
# import functools
#
#
# def auth_dec(func):
#     @functools.wraps(func)
#     def wrapper(*args, **kwargs):
#         name = input('name >> ')
#         password = input('password >> ')
#         if name == 'igor' and password == '1':
#             result = func(*args, **kwargs)
#             return result
#         print('Not allowed')
#     # wrapper.__doc__ = func.__doc__
#     # wrapper.__name__ = func.__name__
#     return wrapper
#
#
# def my_func_myl_3(val):
#     return val * 3
#
#
# my_func_myl_3 = auth_dec(my_func_myl_3)
#
# @auth_dec
# def my_func_myl_4(val):
#     """I multiply all to 4"""
#     return val * 4


# print(my_func_myl_4(9))

# print(my_func_myl_4.__doc__)


# import functools
#
# def auth_dec(param):
#     def auth_dec_(func):
#         @functools.wraps(func)
#         def wrapper(*args, **kwargs):
#             name = input('name >> ')
#             password = input('password >> ')
#             if name == 'igor' and password == '1':
#                 result = func(*args, **kwargs)
#                 return result * param
#             print('Not allowed')
#         # wrapper.__doc__ = func.__doc__
#         # wrapper.__name__ = func.__name__
#         return wrapper
#     return auth_dec_
#
#
# @auth_dec(10)
# def my_func_myl_5(val):
#     """I multiply all to 5"""
#     return val * 5
#
# print(my_func_myl_5(5))

#################################################################################
#  class -> func
from typing import Callable

# no parameter
# class ToTwo:
#     def __init__(self, function: Callable):
#         self.__function = function
#         print(function.__name__)
#
#     def __call__(self, *args, **kwargs):
#         ret_val = str(self.__function(*args, **kwargs)) + '5465656'
#         return ret_val
#
#
# @ToTwo
# def my_func_myl_6(val):
#     """I multiply all to 6"""
#     return val * 6
#
#
# print(my_func_myl_6(6))


# with parameter
# class ToMany:
#     def __init__(self, dating=True):
#         self.__dating = dating
#
#     def __call__(self, function):
#         def wrapper(*args, **kwargs):
#             if self.__dating:
#                 ret_value = f'{function(*args, **kwargs)}, >>>>>  {datetime.date.today()}'
#                 return ret_value
#             return function(*args, **kwargs)
#         return wrapper
#
#
# @ToMany(dating=True)
# def my_func_myl_7(val):
#     """I multiply all to 6"""
#     return val * 7
#
#
# print(my_func_myl_7(6))


#  class -> class

# add attribute

# class Hobby:
#     def __init__(self, naming):
#         self.__naming = naming
#
#     def __call__(self, _class):
#         _class.hobby = self.__naming
#         return _class
#
#
# @Hobby('Dancing')
# class Human:
#     def __init__(self):
#         self.__name = 'igor'
#         self.age = 32
#
#     @property
#     def name(self):
#         return self.__name
#
# igor = Human()
# print(igor.hobby)


# add method and attributes
# from abc import ABC, abstractmethod
#
# # змінюємо атрибути
#
# def tuning(cls, description, price):
#     old_cost = cls.cost
#     old_description = cls.get_description
#
#     def cost(self):
#         new_cost = old_cost(self) + price
#         return new_cost
#
#     def get_description(self):
#         new_description = old_description(self) + description
#         return new_description
#
#     cls.cost = cost
#     cls.get_description = get_description
#     return cls
#
#
# def add_power(cls):
#     def wrapper():
#         return tuning(cls, ' tuned engine, ', 10000)
#     return wrapper()  # with ()   !!!!!
#
#
# def change_color(cls):
#     def wrapper():
#         return tuning(cls, ' color - red,', 1000)
#     return wrapper()  # with ()   !!!!!
#
#
# class Vehicle(ABC):
#     def __init__(self, brand: str, model: str, base_price: int):
#         self._brand = brand
#         self._model = model
#         self._base_price = base_price
#
#     @abstractmethod
#     def cost(self):
#         pass
#
#     @abstractmethod
#     def get_description(self):
#         pass
#
#
# class Auto(Vehicle):
#     def __init__(self):
#         super().__init__('Auto Audi', 'V-60', 50_000)
#
#     def get_description(self):
#         return f'{self._brand}, {self._model}, for {self.cost()}'
#
#     def cost(self):
#         return self._base_price
#
# @add_power
# @add_power
# @add_power
# @add_power
# @change_color
# class F1(Auto):
#     pass
#
#
# @change_color
# @add_power
# @add_power
# @add_power
# @add_power
# class FordGT(Auto):
#     pass
#
#
# @add_power
# class Motorbike(Vehicle):
#     def __init__(self):
#         super().__init__('Motorbike BMW', 'l5', 20_000)
#
#     def get_description(self):
#         return f'{self._brand}, {self._model}, for {self.cost()}'
#
#     def cost(self):
#         return self._base_price
#
# car = Auto()
# print(car.cost())
# print(car.get_description())
# car2 = Auto()
# print(car2.cost())
# print(car2.get_description())
#
# car_f1 = F1()
# print(car_f1.cost())
# print(car_f1.get_description())
#
# car_ford = FordGT()
# print(car_ford.cost())
# print(car_ford.get_description())

import requests
import pprint

url = 'https://dummyjson.com/todos'
request = requests.get(url)

users = {
    'igor',
    'petro',
}


def has_permission(user):
    print('before has_permission')

    def get_func(func):
        def wrapper(*args, **kwargs):
            if user in users:
                return func(*args, **kwargs)
            return 'Not allowed'

        return wrapper

    print('after has_permission')
    return get_func


def serialize_json(func):
    print('before serialise')

    def wrapper(*args, **kwargs):
        raw_data = func(*args, **kwargs)
        data = {
            'data': raw_data['todos'],
            'count': len(raw_data['todos']),
        }
        return data

    print('after serialise')
    return wrapper


user = input('Enter your name >> ')


@has_permission(user)
@serialize_json
def get_todos(request):
    data = request.json()
    print('inside')
    return data


get_todos(request)
